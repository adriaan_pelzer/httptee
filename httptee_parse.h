#ifndef _HTTPTEE_PARSE_H_
#define _HTTPTEE_PARSE_H_

#include "httptee_server.h"

typedef struct httptee_hash {
    char *key;
    char *value;
    /* TODO AT least stick to your own convention! */
} *hh_p;

typedef struct httptee_parsed_result {
#ifdef DEBUG
    char *full_request;
#endif
    char *base_path;
    hh_p query_vars;
    size_t query_var_len;
    /* TODO AT least stick to your own convention! */
} *hpr_p;

void httptee_free_result ( hpr_p result );
hpr_p httptee_parse_result ( const char *get_request );
char *httptee_get_string_after_last_slash ( const char *base_path );

char *httptee_mirror_get_endpoint ( const char *url, const char *separator );

//char *httptee_result_find_value_by_key ( hpr_p result, const char *key );

#endif
