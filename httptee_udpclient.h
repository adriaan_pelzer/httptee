#ifndef _HTTPTEE_UDPCLIENT_H_
#define _HTTPTEE_UDPCLIENT_H_

#include "httptee.h"

int     httptee_open_udp_socket     ();

char    httptee_send_udp_packet     ( int sockfd, const char *data, const char *host, uint16_t port );

void    httptee_close_udp_socket    ( int sockfd );

#endif
