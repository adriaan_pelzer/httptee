#include "httptee_udpclient.h"

static size_t count = 0;

int httptee_open_udp_socket () {
    int sockfd = -1, _sockfd = -1;
    struct sockaddr_in myaddr;
    //int dont_fragment = IP_PMTUDISC_DO;

    if ( ( _sockfd = socket ( AF_INET, SOCK_DGRAM, 0 ) ) == -1 ) {
        syslog ( LOG_ERR, "Cannot open socket: %m" );
        goto over;
    }

    memset ( ( char * ) &myaddr, 0, sizeof ( myaddr ) );
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = htonl ( INADDR_ANY );
    myaddr.sin_port = htons ( 0 );

    if ( bind ( _sockfd, ( struct sockaddr * ) &myaddr, sizeof ( myaddr ) ) == -1 ) {
        syslog ( LOG_ERR, "Cannot bind socket: %m" );
        goto over;
    }

    /*if ( setsockopt ( _sockfd, IPPROTO_IP, IP_MTU_DISCOVER, &dont_fragment, sizeof ( dont_fragment ) ) == -1 ) {
        syslog ( LOG_ERR, "Cannot set socket option IP_MTU_DISCOVER" );
        goto over;
    }*/

    sockfd = _sockfd;
over:
    if ( ( _sockfd != -1 ) && ( sockfd == -1 ) ) {
        close ( _sockfd );
    }

    return sockfd;
}

char httptee_send_udp_packet ( int sockfd, const char *data, const char *host, uint16_t port ) {
    char rc = -1;
    struct hostent *hp;
    struct sockaddr_in servaddr;

    memset ( ( char * ) &servaddr, 0, sizeof ( servaddr ) );
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons ( port );

    if ( ( hp = gethostbyname ( host ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot get host by name: %m" );
        goto over;
    }

    memcpy ( ( void * ) &servaddr.sin_addr, hp->h_addr_list[0], hp->h_length );

    if ( sendto ( sockfd, data, strlen ( data ), 0, ( struct sockaddr * ) &servaddr, sizeof ( servaddr ) ) == -1 ) {
        syslog ( LOG_ERR, "Cannot send packet: %m" );
        goto over;
    }

    syslog ( LOG_DEBUG, "%d packets sent", ( int ) ++count );

    rc = 0;
over:
    endhostent ();
    return rc;
}

void httptee_close_udp_socket ( sockfd ) {
    if ( sockfd != -1 ) close ( sockfd );
}
