#ifndef _HTTPTEE_H_
#define _HTTPTEE_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <nids.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>

#include "httptee_nids_callback.h"
#include "httptee_payloadqueue.h"
#include "httptee_udpclient.h"

#endif
