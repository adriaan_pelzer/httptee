#ifndef _HTTPTEE_PAYLOADQUEUE_H_
#define _HTTPTEE_PAYLOADQUEUE_H_

#include "httptee.h"

#define HTTPTEE_PAYLOADQUEUE_STALECHECK_PERIOD 60

struct httptee_payload {
    size_t  payload_len;
    char    *payload;
};

typedef struct httptee_payloadqueue_entry {
    time_t                              last_processed;
    struct tuple4                       endpoints;
    struct httptee_payload              client;
    struct httptee_payload              server;
    struct httptee_payloadqueue_entry   *prev;
    struct httptee_payloadqueue_entry   *next;
} *pqe_p;

pqe_p httptee_payloadqueue_entry_new                  ( struct tuple4 endpoints );

void  httptee_payloadqueue_entry_free                 ( pqe_p pqe );

char  httptee_add_to_payloadqueue_entry               ( pqe_p pqe, const char *new_payload, const size_t new_payload_len, char client );

pqe_p httptee_find_payloadqueue_entry_by_endpoints    ( pqe_p pq, struct tuple4 endpoints );

pqe_p httptee_add_to_payloadqueue                     ( pqe_p pq, struct tuple4 endpoints, const char *payload, const size_t payload_len, char client );

char  httptee_payload_splice                          ( struct httptee_payload *payload, size_t start_index, size_t end_index );

void  httptee_remove_entry_from_payloadqueue          ( pqe_p *pq, pqe_p entry );

char  httptee_remove_from_payloadqueue                ( pqe_p *pq, struct tuple4 endpoints );

#ifdef REMOVE_STALE_QUEUES
void  httptee_remove_stale_payloads                   ( pqe_p *pq );
#endif

pqe_p httptee_find_stale_payload                      ( pqe_p pq );

#endif
