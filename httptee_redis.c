#include "httptee_redis.h"

static size_t bufTime = 3600;
static size_t dbNumber = 0;

statItem_p httptee_redis_statitem_extract ( hpr_p result ) {
    statItem_p rc = NULL, _rc = NULL;
    size_t i = 0;
    char empty = 0;

    if ( ( _rc = malloc ( sizeof ( struct statItem ) ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for statItem" );
        goto over;
    }

    memset ( _rc, 0, sizeof ( struct statItem ) );

    _rc->time = time ( NULL );

    for ( i = 0; i < result->query_var_len; i++ ) {
        char *key = result->query_vars[i].key;
        char *value = result->query_vars[i].value;
        char **populatee = NULL;

        if ( ! ( strncmp ( key, "events", 6 ) ) ) {
            populatee = &_rc->events;
        } else if ( ! ( strncmp ( key, "c19", 3 ) ) ) {
            populatee = &_rc->c19;
        /*} else if ( ! ( strncmp ( key, "v50", 3 ) ) ) {
            populatee = &_rc->v50;*/
        } else if ( ! ( strncmp ( key, "v54", 3 ) ) ) {
            populatee = &_rc->v54;
        /*} else if ( ! ( strncmp ( key, "pageName", 8 ) ) ) {
            populatee = &_rc->pageName;*/
        }

        if ( populatee ) {
            empty = 0;

            if ( ( *populatee = malloc ( strlen ( value ) + 1 ) ) == NULL ) {
                syslog ( LOG_ERR, "Cannot allocate memory for statItem events" );
                goto over;
            }

            snprintf ( *populatee, strlen ( value ) + 1, "%s", value );
        }
    }

    if ( empty ) {
        goto over;
    }

    rc = _rc;
over:
    if ( _rc && ( rc == NULL ) ) {
        httptee_redis_statitem_free ( _rc );
    }

    return rc;
}

void httptee_redis_statitem_free ( statItem_p statItem ) {
    if ( statItem ) {
        //if ( statItem->pageName ) free ( statItem->pageName );
        if ( statItem->c19 ) free ( statItem->c19 );
        //if ( statItem->v50 ) free ( statItem->v50 );
        if ( statItem->v54 ) free ( statItem->v54 );
        if ( statItem->events ) free ( statItem->events );
        free ( statItem );
    }
}

redisContext *httptee_redis_connect ( const char *ip, size_t port, size_t bt, size_t dbNum ) {
    redisContext *rc = NULL, *_rc = NULL;

    bufTime = bt;
    dbNumber = dbNum;

    srand ( time ( NULL ) );

    if ( ( _rc = redisConnect ( ip, ( int ) port ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot create redis connection" );
        goto over;
    }

    if ( _rc->err ) {
        syslog ( LOG_ERR, "Cannot create redis connection: %s", _rc->errstr );
        goto over;
    }

    if ( send_redis_command ( _rc, REDIS_REPLY_STATUS, NULL, NULL, "SELECT %d", ( int ) dbNum ) == 0 ) {
        syslog ( LOG_ERR, "Cannot select database %d", ( int ) dbNum );
        goto over;
    }

    rc = _rc;
over:
    if ( _rc && ( rc == NULL ) ) {
        redisFree ( _rc );
    }

    return rc;
}

char send_redis_command ( redisContext *redis_ctx, int expected_reply_type, void *reply, size_t *reply_len, const char *format, ... ) {
    char rc = 0;
    char command[4096];
    va_list argptr;
    redisReply *rep = NULL;

    va_start ( argptr, format );
    vsnprintf ( command, 4096, format, argptr );
    va_end ( argptr );

    if ( ( rep = redisCommand ( redis_ctx, command ) ) == NULL ) {
        if ( ( redis_ctx = httptee_redis_connect ( "127.0.0.1", 6379, bufTime, dbNumber ) ) == NULL ) {
            syslog ( LOG_ERR, "Cannot reconnect to redis" );
            goto over;
        }

        if ( ( rep = redisCommand ( redis_ctx, command ) ) == NULL ) {
            syslog ( LOG_ERR, "Cannot send redis command: '%s': %s", command, redis_ctx->errstr );
            goto over;
        }
    }

    if ( rep->type != expected_reply_type ) {
        syslog ( LOG_ERR, "'%s' returned the wrong type: (%d should be %d)", command, rep->type, expected_reply_type );
        goto over;
    }

    switch ( rep->type ) {
        case REDIS_REPLY_STRING:
        case REDIS_REPLY_ERROR:
            if ( reply_len ) {
                *reply_len = rep->len;
            }

            if ( reply ) {
                char *_reply = NULL;

                if ( ( _reply = strndup ( rep->str, rep->len ) ) == NULL ) {
                    syslog ( LOG_ERR, "Cannot copy reply string: %m" );
                    goto over;
                }

                reply = &_reply;
            }

            break;
        case REDIS_REPLY_STATUS:
            if ( ( rep->len != 2 ) || strncmp ( rep->str, "OK", 2 ) ) {
                char reply_str[rep->len + 1];

                snprintf ( reply_str, rep->len + 1, "%s", rep->str );
                syslog ( LOG_ERR, "Redis reply is not 'OK': (%s)", reply_str );
                goto over;
            }

            break;
        case REDIS_REPLY_INTEGER:
            if ( reply ) {
                * ( ( long long * ) reply ) = rep->integer;
            }

            break;
        case REDIS_REPLY_ARRAY:
            syslog ( LOG_ERR, "ARRAY not implemented yet" );
            goto over;

            break;
        case REDIS_REPLY_NIL:
            break;
    }

    rc = 1;
over:
    if ( rep ) {
        freeReplyObject ( rep );
    }

    return rc;
}

char httptee_redis_expire_stale_result ( redisContext *redis_ctx, const char *c19, const char *events ) {
    char rc = 0;

    if ( send_redis_command ( redis_ctx, REDIS_REPLY_INTEGER, NULL, NULL, "ZREMRANGEBYSCORE %s:%s -inf %d", c19, events, time ( NULL ) - bufTime ) == 0 ) {
        syslog ( LOG_ERR, "Cannot ZREMRANGEBYSCORE" );
        goto over;
    }

    rc = 1;
over:
    return rc;
}

char httptee_redis_set_expire ( redisContext *redis_ctx, const char *c19, const char *item, const char *value ) {
    char rc = 0;
    long long expire_status = 0;

    if ( send_redis_command ( redis_ctx, REDIS_REPLY_STATUS, NULL, NULL, "SET %s:%s %s", c19, item, value ) == 0 ) {
        syslog ( LOG_ERR, "Cannot SET '%s:%s': %s", c19, item, redis_ctx->errstr );
        goto over;
    }

    if ( send_redis_command ( redis_ctx, REDIS_REPLY_INTEGER, &expire_status, NULL, "EXPIRE %s:%s %d", c19, item, bufTime ) == 0 ) {
        syslog ( LOG_ERR, "Cannot expire '%s:%s': %s", c19, item, redis_ctx->errstr );
        goto over;
    }

    if ( expire_status != 1 ) {
        syslog ( LOG_ERR, "Cannot expire '%s:%s': %s", c19, item, redis_ctx->errstr );
        goto over;
    }

    rc = 1;
over:
    return rc;
}

char httptee_redis_add_result ( redisContext *redis_ctx, hpr_p result ) {
    char rc = 0;
    statItem_p statItem = NULL;
    long long status = 0;

    if ( ( statItem = httptee_redis_statitem_extract ( result ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot extract statItem from result" );
        goto over;
    }

    if ( statItem->events && statItem->c19 && /*statItem->pageName && */ statItem->v54 && statItem->time ) {
        /*if ( httptee_redis_set_expire ( redis_ctx, statItem->c19, "pageName", statItem->pageName ) == 0 ) {
            syslog ( LOG_ERR, "Cannot SET and EXPIRE" );
            goto over;
        }*/

        /*if ( statItem->v50 ) {
            if ( httptee_redis_set_expire ( redis_ctx, statItem->c19, "v50", statItem->v50 ) == 0 ) {
                syslog ( LOG_ERR, "Cannot SET and EXPIRE" );
                goto over;
            }
        }*/

        if ( statItem->v54 ) {
            if ( httptee_redis_set_expire ( redis_ctx, statItem->c19, "v54", statItem->v54 ) == 0 ) {
                syslog ( LOG_ERR, "Cannot SET and EXPIRE" );
                goto over;
            }
        }

        if ( send_redis_command ( redis_ctx, REDIS_REPLY_INTEGER, &status, NULL, "ZADD %s:%s %d %s", statItem->c19, statItem->events, statItem->time, httptee_get_string_after_last_slash ( result->base_path ) ) == 0 ) {
            syslog ( LOG_ERR, "Cannot ZADD" );
            goto over;
        }

        if ( status != 1 ) {
            //syslog ( LOG_ERR, "ZADD failed" );
            goto over;
        }

        if ( send_redis_command ( redis_ctx, REDIS_REPLY_INTEGER, &status, NULL, "EXPIRE %s:%s %d", statItem->c19, statItem->events, bufTime ) == 0 ) {
            syslog ( LOG_ERR, "Cannot expire '%s:%s': %s", statItem->c19, statItem->events, redis_ctx->errstr );
            goto over;
        }

        if ( status != 1 ) {
            syslog ( LOG_ERR, "Cannot expire '%s:%s': %s", statItem->c19, statItem->events, redis_ctx->errstr );
            goto over;
        }

        if ( httptee_redis_expire_stale_result ( redis_ctx, statItem->c19, statItem->events ) == 0 ) {
            syslog ( LOG_ERR, "Cannot remove stale entries from %s:%s", statItem->c19, statItem->events );
            goto over;
        }
    }

    rc = 1;
over:
    if ( statItem ) {
        httptee_redis_statitem_free ( statItem );
    }

    return rc;
}

void httptee_redis_free ( redisContext *redis_ctx ) {
    redisFree ( redis_ctx );
}
