#include "httptee.h"

#define DAEMON_NAME "httptee"
#define PIDFILE "/var/run/httptee.pid"

void PrintUsage ( int argc, char **argv ) {
    if ( argc > 0 ) {
        printf ( "Usage: %s\n", argv[0] );
        printf ( "      -i <interface>\n" );
        printf ( "      -s <server address>\n" );
        printf ( "      -p <server port>\n" );
        printf ( "      -f <filter string>\n" );
        printf ( "      -h\n" );
        printf ( "      -n\n" );
        printf ( "  Options:\n" );
        printf ( "      interface:\tInterface name to listen on (default: all)\n" );
        printf ( "      server address:\tServer Address (where UDP packets are sent to)\n" );
        printf ( "      server port:\tServer Port\n" );
        printf ( "      filter string:\tFilter string in pcap format\n" );
        printf ( "      -n\t\tDon't daemonise.\n" );
        printf ( "      -h\t\tShow this help screen.\n" );
        printf ( "\n" );
        printf ( "  Listens for HTTP GET requests, and sends them via UDP to a server\n" );
    }
}

void signal_handler ( int sig ) {
    switch ( sig ) {
        case SIGHUP:
            syslog ( LOG_WARNING, "Received %s signal. (not implemented)", strsignal ( sig ) );
            break;
        case SIGINT:
        case SIGABRT:
        case SIGTERM:
            syslog ( LOG_WARNING, "Received %s signal.", strsignal ( sig ) );
            unlink ( PIDFILE );
            exit ( EXIT_SUCCESS );
            break;
        default:
            syslog ( LOG_WARNING, "Unhandled signal (%d) %s", sig, strsignal ( sig ) );
            break;
    }
}

int main ( int argc, char **argv ) {
    int rc = EXIT_FAILURE, c, p = 0;
    char device[16];
    char pcap_filter[256];
    char server_set = 0;
    struct nids_chksum_ctl nochksumchk;
    pid_t pid, sid;

#ifdef DEBUG
    int daemonize = 0;
#else
    int daemonize = 1;
#endif

    signal ( SIGHUP, signal_handler );
    signal ( SIGTERM, signal_handler );
    signal ( SIGINT, signal_handler );
    signal ( SIGABRT, signal_handler );

#ifdef DEBUG
    setlogmask ( LOG_UPTO ( LOG_DEBUG ) );
    openlog ( DAEMON_NAME, LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER );
#else
    setlogmask ( LOG_UPTO ( LOG_INFO ) );
    openlog ( DAEMON_NAME, LOG_CONS, LOG_USER );
#endif

    device[0] = '\0';
    pcap_filter[0] = '\0';

    while ( ( c = getopt(argc, argv, "i:s:p:f:nh|help" ) ) != -1 ) {
        switch ( c ) {
            case 'i':
                if ( strlen ( optarg ) > 15 ) {
                    fprintf ( stderr, "Please keep the interface name below 15 characters\n" );
                    goto over;
                }
                snprintf ( device, 256, "%s", optarg );
                break;
            case 's':
                if ( strlen ( optarg ) > 255 ) {
                    fprintf ( stderr, "Please keep the server address shorter than 255 characters\n" );
                    goto over;
                }

                httptee_set_server_address ( optarg );
                server_set = 1;
                break;
            case 'p':
                p = atoi ( optarg );

                if ( p < 1 || p > 65535 ) {
                    fprintf ( stderr, "Please keep the server port between 1 and 65535\n" );
                    goto over;
                }

                httptee_set_server_port ( p );
                break;
            case 'f':
                if ( strlen ( optarg ) > 255 ) {
                    fprintf ( stderr, "Please keep the filter string below 256 characters\n" );
                    goto over;
                }
                snprintf ( pcap_filter, 256, "%s", optarg );
                break;
            case 'h':
                PrintUsage ( argc, argv );
                goto over;
                break;
            case 'n':
                daemonize = 0;
                break;
            default:
                PrintUsage ( argc, argv );
                goto over;
                break;
        }
    }

    /* Validate input */
    if ( strlen ( device ) == 0 ) snprintf ( device, 256, "all" );

    if ( ! server_set ) {
        fprintf ( stderr, "Please specify a server address\n" );
        goto over;
    }

    if ( p == 0 ) {
        fprintf ( stderr, "Please set a port number\n" );
        goto over;
    }
    /******************/

    if ( daemonize ) {
        syslog ( LOG_INFO, "%s daemon starting up", DAEMON_NAME );

        if ( ( pid = fork () ) < 0 ) {
            syslog ( LOG_ERR, "Could not fork child process: %m" );
            goto over;
        } else if ( pid > 0 ) {
            FILE *pidfp = NULL;
            char pid_string[256];

            if ( ( pidfp = fopen ( PIDFILE, "w" ) ) ) {
                snprintf ( pid_string, 256, "%d", pid );
                fwrite ( pid_string, strlen ( pid_string ), 1, pidfp );
                fclose ( pidfp );
            }

            syslog ( LOG_NOTICE, "Child process successfully forked" );
            rc = EXIT_SUCCESS;

            goto over;
        }

        umask ( 0 );

        if ( ( sid = setsid () ) < 0 ) {
            syslog ( LOG_ERR, "Could not create new session: %m" );
            goto over;
        }

        if ( ( chdir ( "/" ) ) < 0 ) {
            syslog ( LOG_ERR, "Could not change working dir: %m" );
            goto over;
        }

        close ( STDIN_FILENO );
        close ( STDOUT_FILENO );
        close ( STDERR_FILENO );
    }

    nids_params.device = device;
    nids_params.pcap_filter = pcap_filter;
#ifdef DEBUG
    nids_params.syslog_level = LOG_DEBUG;
#endif

    if ( ! nids_init () ) {
        syslog ( LOG_ERR, "%s", nids_errbuf );
        goto over;
    }

    nochksumchk.netaddr = 0;
    nochksumchk.mask = 0;
    nochksumchk.action = NIDS_DONT_CHKSUM;

    nids_register_chksum_ctl ( &nochksumchk, 1 );

    nids_register_tcp ( httptee_nids_tcp_handler );

    nids_run ();

    rc = EXIT_SUCCESS;
over:
    return rc;
}
