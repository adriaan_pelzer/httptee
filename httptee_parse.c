#include "httptee_redis.h"
#include "httptee_parse.h"

void httptee_free_hash ( struct httptee_hash hash ) {
    if ( hash.key ) free ( hash.key );
    if ( hash.value ) free ( hash.value );
}

hpr_p httptee_create_empty_result () {
    hpr_p rc = NULL, _rc = NULL;

    if ( ( _rc = malloc ( sizeof ( struct httptee_parsed_result ) ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for new empty result: %m" );
        goto over;
    }

    memset ( _rc, 0, sizeof ( struct httptee_parsed_result ) );

    rc = _rc;
over:
    if ( _rc && ( rc == NULL ) ) {
        httptee_free_result ( _rc );
    }

    return rc;
}

#ifdef DEBUG
char httptee_replace_full_request ( hpr_p result, const char *request ) {
    char rc = 0;

    if ( result->full_request ) {
        free ( result->full_request );
        result->full_request = NULL;
    }

    if ( ( result->full_request = malloc ( strlen ( request ) + 1 ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for full request: %m" );
        goto over;
    }

    snprintf ( result->full_request, strlen ( request ) + 1, "%s", request );

    rc = 1;
over:
    return rc;
}
#endif

char httptee_replace_base_path ( hpr_p result, const char *path ) {
    char rc = 0;

    if ( result->base_path ) {
        free ( result->base_path );
        result->base_path = NULL;
    }

    /* TODO don't allocate memory */
    if ( ( result->base_path = malloc ( strlen ( path ) + 1 ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for base path: %m" );
        goto over;
    }

    snprintf ( result->base_path, strlen ( path ) + 1, "%s", path );

    rc = 1;
over:
    return rc;
}

char httptee_add_to_result ( hpr_p result, const char *key, const char *value ) {
    char rc = 0;

    if ( ( result->query_vars = realloc ( result->query_vars, ( result->query_var_len + 1 ) * sizeof ( struct httptee_hash ) ) ) == NULL ) {
        syslog ( LOG_CRIT, "Cannot reallocate memory for hash entry inside result: %m" );
        goto over;
    }

    if ( ( result->query_vars[result->query_var_len].key = malloc ( strlen ( key ) + 1 ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for new key" );
        goto over;
    }

    if ( ( result->query_vars[result->query_var_len].value = malloc ( strlen ( value ) + 1 ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for new value" );
        goto over;
    }

    snprintf ( result->query_vars[result->query_var_len].key, strlen ( key ) + 1, "%s", key );
    snprintf ( result->query_vars[result->query_var_len].value, strlen ( value ) + 1, "%s", value );

    rc = 1;
    result->query_var_len++;
over:
    return rc;
}

void httptee_free_result ( hpr_p result ) {
    size_t i = 0;

    if ( result ) {
        if ( result->base_path ) free ( result->base_path );
#ifdef DEBUG
        if ( result->full_request ) free ( result->full_request );
#endif
        if ( result->query_vars ) {
            for ( i = 0; i < result->query_var_len; i++ ) {
                httptee_free_hash ( result->query_vars[i] );
            }

            free ( result->query_vars );
        }

        free ( result );
    }
}

char *httptee_get_string_after_last_slash ( const char *base_path ) {
    char *rc = NULL;
    size_t i = 0;

    for ( i = 0; i < strlen ( base_path ); i++ ) {
        if ( base_path[i] == '/' ) {
            rc = ( char * ) ( ( size_t ) base_path + i + 1 );
        }
    }

    return rc;
}

hpr_p httptee_parse_result ( const char *get_request_url ) {
    hpr_p rc = NULL, _rc = NULL;
    char *request_copy = NULL;
    size_t i = 0, key_marker = 0, value_marker = 0, request_len = strlen ( get_request_url );

    if ( ( request_copy = malloc ( request_len + 1 ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for request copy: %m" );
        goto over;
    }

    snprintf ( request_copy, request_len + 1, "%s", get_request_url );

    if ( ( _rc = httptee_create_empty_result () ) == NULL ) {
        syslog ( LOG_ERR, "Cannot create empty result" );
        goto over;
    }

#ifdef DEBUG
    if ( httptee_replace_full_request ( _rc, get_request_url ) == 0 ) {
        syslog ( LOG_ERR, "Cannot add full request" );
        goto over;
    }
#endif

    for ( i = 0; i < request_len; i++ ) {
        switch ( request_copy[i] ) {
            case '?':
                request_copy[i] = '\0';
                key_marker = i + 1;

                if ( httptee_replace_base_path ( _rc, request_copy ) == 0 ) {
                    syslog ( LOG_ERR, "Cannot add base path" );
                    goto over;
                }

                break;
            case '&':
                request_copy[i] = '\0';

                if ( key_marker < i ) {
                    if ( httptee_add_to_result ( _rc, ( char * ) ( ( size_t ) request_copy + key_marker ), ( char * ) ( ( size_t ) request_copy + value_marker ) ) == 0 ) {
                        syslog ( LOG_ERR, "Cannot add new hash to result" );
                        goto over;
                    }
                }

                key_marker = i + 1;

                break;
            case '=':
                request_copy[i] = '\0';
                value_marker = i + 1;

                break;
        }
    }

    if ( key_marker < i ) {
        if ( httptee_add_to_result ( _rc, ( char * ) ( ( size_t ) request_copy + key_marker ), ( char * ) ( ( size_t ) request_copy + value_marker ) ) == 0 ) {
            syslog ( LOG_ERR, "Cannot add new hash to result" );
            goto over;
        }
    }

    rc = _rc;
over:
    if ( _rc && ( rc == NULL ) ) {
        httptee_free_result ( _rc );
    }

    /* TODO don't free */
    if ( request_copy ) {
        free ( request_copy );
    }

    return rc;
}

char *httptee_mirror_get_endpoint ( const char *url, const char *separator ) {
    char *rc = ( char * ) url;
    size_t i = 0, j = 0;
    char match = 0;

    for ( i = 0; i < strlen ( url ); i++ ) {
        match = 1;

        for ( j = 0; j < strlen ( separator ); j++ ) {
            if ( url[i - ( strlen ( separator ) - 1 ) + j] != separator[j] ) {
                match = 0;
                break;
            }
        }

        if ( match ) {
            rc = ( char * ) ( ( size_t ) url + i + 1 );
        }
    }

    return rc;
}

/* TODO remove if not used */
/*char *httptee_result_find_value_by_key ( hpr_p result, const char *key ) {
    char *rc = NULL;
    size_t i = 0;

    for ( i = 0; i < result->query_var_len; i++ ) {
        if ( ! ( strncmp ( result->query_vars[i].key, key, strlen ( key ) ) ) ) {
            rc = result->query_vars[i].value;
            break;
        }
    }

    return rc;
}*/
