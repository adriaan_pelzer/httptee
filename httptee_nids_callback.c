#include "httptee_nids_callback.h"

static pqe_p payloadqueue = NULL;
#ifdef REMOVE_STALE_QUEUES
static time_t stale_queues_last_removed = 0;
#endif
#ifdef DEBUG
static size_t count_gets = 0;
#endif
static int udp_socket = -1;
static char server_address[256];
static int server_port = 0;

void httptee_set_server_address ( const char *address ) {
    snprintf ( server_address, 256, "%s", address );
}

void httptee_set_server_port ( int port ) {
    server_port = port;
}

int string_index_in_binary ( const char *needle, const char *haystack, const size_t haystack_len ) {
    int rc = -1;
    size_t i = 0, needle_found_state = 0;

    for ( i = 0; i < haystack_len; i++ ) {
        if ( needle_found_state < strlen ( needle ) ) {
            if ( haystack[i] == needle[needle_found_state] ) {
                needle_found_state++;
            } else {
                needle_found_state = 0;
            }
        } else {
            rc = i - strlen ( needle );
            goto over;
        }
    }

    if ( needle_found_state > 0 ) {
        rc = haystack_len - needle_found_state;
    }

over:
    return rc;
}

/*
 * Returns a null-terminated substring of haystack (non-null-terminated), which starts
 * with string needle_start (exclusive) and ends with needle_end (exclusive).
 * Returns NULL id either needle_start or needle_end is not found.
 *
 * It allocates memory for the returned string, which has to be freed by the caller.
 *
 * It also places the end index into the address pointed to by end, useful for splicing
 * the haystack after processing.
 */

char *find_string_in_binary ( const char *needle_start, const char *needle_end, const char *haystack, const size_t haystack_len, size_t *end ) {
    char *rc = NULL, *_rc = NULL;
    size_t i = 0, start_found_state = 0, end_found_state = 0;
    size_t start_index = -1, end_index = -1;

    for ( i = 0; ( i < haystack_len ) && ( end_index == -1 ); i++ ) {
        if ( start_found_state < strlen ( needle_start ) ) {
            if ( haystack[i] == needle_start[start_found_state] ) {
                start_found_state++;
            } else {
                start_found_state = 0;
            }
        } else {
            if ( start_index == -1 ) {
                start_index = i;
            }

            if ( end_found_state < strlen ( needle_end ) ) {
                if ( haystack[i] == needle_end[end_found_state] ) {
                    end_found_state++;
                } else {
                    end_found_state = 0;
                }
            } else {
                if ( end_index == -1 ) {
                    end_index = i - strlen ( needle_end );
                }
            }
        }
    }

    if ( end_index == -1 ) {
        goto over;
    }

    if ( ( _rc = malloc ( end_index - start_index + 1 ) ) == NULL ) {
        syslog ( LOG_ERR, "Not enough memory" );
        goto over;
    }

    snprintf ( _rc, end_index - start_index + 1, "%s", haystack + start_index );

    rc = _rc;
    *end = end_index;
over:
    if ( _rc && ( rc == NULL ) ) {
        free ( _rc );
    }

    return rc;
}

char *httptee_payload_processor ( struct httptee_payload *entity ) {
    char *rc = NULL, *_rc = NULL;
    size_t end_index;
    int get_index = -1;

    if ( entity->payload_len == 0 ) {
        goto over;
    }

    if ( ( _rc = find_string_in_binary ( "GET ", " HTTP/", entity->payload, entity->payload_len, &end_index ) ) == NULL ) {
        if ( ( get_index = string_index_in_binary ( "GET ", entity->payload, entity->payload_len ) ) == -1 ) {
            if ( entity->payload ) free ( entity->payload );
            entity->payload = NULL;
            entity->payload_len = 0;
        } else {
            if ( httptee_payload_splice ( entity, 0, ( size_t ) get_index ) == 0 ) {
                syslog ( LOG_ERR, "Cannot splice entity" );
            }
        }
            
        goto over;
    }

    if ( httptee_payload_splice ( entity, 0, end_index ) == 0 ) {
        syslog ( LOG_ERR, "Cannot splice payload" );
        goto over;
    }

    rc = _rc;
over:
    if ( _rc && ( rc == NULL ) ) {
        free ( _rc );
    }

    return rc;
}

char *httptee_payloadqueue_entry_processor ( pqe_p pqe ) {
    char *rc = NULL, *_rc = NULL;

    if ( pqe->client.payload_len ) {
        /* Should never trigger */
        syslog ( LOG_ERR, "There's something in the client flow - this is wrong" );
    }

    if ( pqe->server.payload_len ) {
        if ( ( _rc = httptee_payload_processor ( &pqe->server ) ) == NULL ) {
            goto over;
        }
    }

    pqe->last_processed = time ( NULL );
    rc = _rc;
over:
    if ( _rc && ( rc == NULL ) ) {
        free ( _rc );
    }

    return rc;
}

#ifdef DEBUG
void print_stats ( pqe_p pq ) {
    size_t num_queues = 0;
    size_t num_chars_client = 0;
    size_t num_chars_server = 0;
    pqe_p next = pq;

    while ( next ) {
        num_queues++;
        num_chars_client += next->client.payload_len;
        num_chars_server += next->server.payload_len;
        next = next->next;
    }

    syslog ( LOG_DEBUG, "----------------------------------------" );
    syslog ( LOG_DEBUG, "Number of queues: %d", ( int ) num_queues );
    syslog ( LOG_DEBUG, "Number of client bytes: %d", ( int ) num_chars_client );
    syslog ( LOG_DEBUG, "Number of server bytes: %d", ( int ) num_chars_server );
    syslog ( LOG_DEBUG, "----------------------------------------" );
}
#endif
    
void httptee_nids_tcp_handler ( struct tcp_stream *tcp_stream, void **this_time_not_needed ) {
    char addresses[LOG_MAX];
    struct in_addr source_addr, dest_addr;

#ifdef REMOVE_STALE_QUEUES
    if ( stale_queues_last_removed == 0 ) stale_queues_last_removed = time ( NULL );

    if ( ( time ( NULL ) - stale_queues_last_removed ) > HTTPTEE_PAYLOADQUEUE_STALECHECK_PERIOD ) {
#ifdef DEBUG
        print_stats ( payloadqueue );
#endif
        httptee_remove_stale_payloads ( &payloadqueue );
        stale_queues_last_removed = time ( NULL );
#ifdef DEBUG
        print_stats ( payloadqueue );
#endif
    }
#endif

    source_addr.s_addr = tcp_stream->addr.saddr;
    dest_addr.s_addr = tcp_stream->addr.daddr;

    snprintf ( addresses, LOG_MAX, "%s:%d -> ", inet_ntoa ( source_addr ), tcp_stream->addr.source );
    snprintf ( addresses + strlen ( addresses ), LOG_MAX - strlen ( addresses ), "%s:%d", inet_ntoa ( dest_addr ), tcp_stream->addr.dest );

    switch ( tcp_stream->nids_state ) {
        case NIDS_JUST_EST:
            tcp_stream->client.collect++;
            tcp_stream->client.collect_urg++;
            tcp_stream->server.collect++;
            tcp_stream->server.collect_urg++;

            fprintf ( stdout, "%s established\n", addresses );

            goto over;

        case NIDS_CLOSE:
            fprintf ( stdout, "%s closing\n", addresses );
            if ( httptee_remove_from_payloadqueue ( &payloadqueue, tcp_stream->addr ) == 0 ) {
                syslog ( LOG_WARNING, "CLOSE: Cannot remove entry from payloadqueue" );
            }
            goto over;

        case NIDS_RESET:
            fprintf ( stdout, "%s reset\n", addresses );
            if ( httptee_remove_from_payloadqueue ( &payloadqueue, tcp_stream->addr ) == 0 ) {
                syslog ( LOG_WARNING, "RESET: Cannot remove entry from payloadqueue" );
            }
            goto over;

        case NIDS_DATA:
            {
                struct half_stream *hlf;
                char client = 0;

                if ( tcp_stream->server.count_new_urg ) {
                    goto over;
                }

                if ( tcp_stream->client.count_new_urg ) {
                    goto over;
                }

                if ( tcp_stream->client.count_new ) {
                    hlf = &tcp_stream->client;
                    client = 1;
                } else {
                    hlf = &tcp_stream->server;
                    client = 0;
                }

                if ( client == 0 ) {
                    pqe_p pqe = NULL;

                    if ( payloadqueue == NULL ) {
                        if ( ( payloadqueue = httptee_payloadqueue_entry_new ( tcp_stream->addr ) ) == NULL ) {
                            syslog ( LOG_ERR, "Cannot create payload queue" );
                            goto over;
                        }
                    }

                    if ( ( pqe = httptee_add_to_payloadqueue ( payloadqueue, tcp_stream->addr, hlf->data, hlf->count_new, client ) ) == NULL ) {
                        syslog ( LOG_ERR, "Cannot add data to queue" );
                    } else {
                        char *slice = NULL;

                        if ( ( slice = httptee_payloadqueue_entry_processor ( pqe ) ) ) {
                            if ( udp_socket == -1 ) {
                                if ( ( udp_socket = httptee_open_udp_socket () ) == -1 ) {
                                    syslog ( LOG_ERR, "Cannot open UDP socket" );
                                    goto over;
                                }
                            }

                            if ( udp_socket != -1 ) {
                                if ( httptee_send_udp_packet ( udp_socket, slice, server_address, server_port ) == -1 ) {
                                    syslog ( LOG_ERR, "Cannot send UDP packet" );
                                    goto over;
                                }
                            }

#ifdef DEBUG
                            count_gets++;
                            syslog ( LOG_DEBUG, "%d GETS found so far", count_gets );
#endif
                            syslog ( LOG_DEBUG, "%s", slice );
                            syslog ( LOG_DEBUG, "-------------------------------------------" );

                            free ( slice );
                        }
                    }
                }
            }

            goto over;
    }

over:
    return;
}
