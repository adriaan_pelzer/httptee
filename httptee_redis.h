#ifndef _HTTPTEE_REDIS_H_
#define _HTTPTEE_REDIS_H_

#include "httptee_server.h"

typedef struct statItem {
    //char *pageName;
    char *c19; /* article ID */
    //char *v50; /* image URL */
    char *v54; /* article URL */
    char *events;
    time_t time;
} *statItem_p;

statItem_p httptee_redis_statitem_extract ( hpr_p result );

void httptee_redis_statitem_free ( statItem_p );

redisContext *httptee_redis_connect ( const char *ip, size_t port, size_t bufTime, size_t dbNum );

char httptee_redis_add_result ( redisContext *redis_ctx, hpr_p result );

void httptee_redis_free ( redisContext *redis_ctx );

char send_redis_command ( redisContext *redis_ctx, int expected_reply_type, void *reply, size_t *reply_len, const char *format, ... );

#endif
