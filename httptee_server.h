#ifndef _HTTPTEE_SERVER_H_
#define _HTTPTEE_SERVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <string.h>
#include <signal.h>
#include <syslog.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <hiredis/hiredis.h>
#include <time.h>
#include <stdarg.h>

#include "httptee_parse.h"
#include "httptee_redis.h"

#endif
