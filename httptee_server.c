#include "httptee_server.h"

#define DAEMON_NAME "httptee_server"
#define PIDFILE "/var/run/httptee_server.pid"

/* TODO find out how to calculate optimal */
#define BUFSIZE 8192

void PrintUsage ( int argc, char **argv ) {
    if ( argc > 0 ) {
        printf ( "Usage: %s\n", argv[0] );
        printf ( "      -p <port>\n" );
        printf ( "      -h\n" );
        printf ( "      -n\n" );
        printf ( "  Options:\n" );
        printf ( "      port:\tPort to listen on\n" );
        printf ( "      -n\t\tDon't daemonise.\n" );
        printf ( "      -h\t\tShow this help screen.\n" );
        printf ( "\n" );
        printf ( "  Consumes relayed GET requests, sent via UDP\n" );
    }
}

void signal_handler ( int sig ) {
    switch ( sig ) {
        case SIGHUP:
            syslog ( LOG_WARNING, "Received %s signal. (not implemented)", strsignal ( sig ) );
            break;
        case SIGINT:
        case SIGABRT:
        case SIGTERM:
            syslog ( LOG_WARNING, "Received %s signal.", strsignal ( sig ) );
            unlink ( PIDFILE );
            exit ( EXIT_SUCCESS );
            break;
        default:
            syslog ( LOG_WARNING, "Unhandled signal (%d) %s", sig, strsignal ( sig ) );
            break;
    }
}

int setup_server ( int port ) {
    int sockfd = -1, _sockfd = -1;
    struct sockaddr_in myaddr;

    if ( ( _sockfd = socket ( AF_INET, SOCK_DGRAM, 0 ) ) < 0 ) {
        syslog ( LOG_ERR, "Cannot create socket: %m" );
        goto over;
    }

    memset ( ( char * ) &myaddr, 0, sizeof ( myaddr ) );
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = htonl ( INADDR_ANY );
    myaddr.sin_port = htons ( port );

    if ( bind ( _sockfd, ( struct sockaddr * ) &myaddr, sizeof ( myaddr ) ) < 0 ) {
        syslog ( LOG_ERR, "Cannot bind socket: %m" );
        goto over;
    }

    sockfd = _sockfd;
over:
    if ( ( _sockfd != -1 ) && ( sockfd == -1 ) ) {
        close ( _sockfd );
    }

    return sockfd;
}

int main ( int argc, char **argv ) {
    int rc = EXIT_FAILURE, c, sockfd = -1, recvlen;
    size_t p = 0, bufTime = 3600, dbNum = 0; 
    pid_t pid, sid;
    char server_buffer[BUFSIZE];
    struct sockaddr_in remaddr;
    socklen_t addrlen = sizeof ( remaddr );
    redisContext *redis_ctx = NULL;

#ifdef DEBUG
    int daemonize = 0;
#else
    int daemonize = 1;
#endif

    signal ( SIGHUP, signal_handler );
    signal ( SIGTERM, signal_handler );
    signal ( SIGINT, signal_handler );
    signal ( SIGABRT, signal_handler );

#ifdef DEBUG
    setlogmask ( LOG_UPTO ( LOG_DEBUG ) );
    openlog ( DAEMON_NAME, LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER );
#else
    setlogmask ( LOG_UPTO ( LOG_INFO ) );
    openlog ( DAEMON_NAME, LOG_CONS, LOG_USER );
#endif

    while ( ( c = getopt(argc, argv, "d:p:t:nh|help" ) ) != -1 ) {
        switch ( c ) {
            case 'p':
                p = atoi ( optarg );

                if ( p < 1 || p > 65535 ) {
                    fprintf ( stderr, "Please keep the server port between 1 and 65535\n" );
                    goto over;
                }

                break;
            case 't':
                bufTime = atoi ( optarg );

                if ( bufTime < 1 || bufTime > 604800 ) {
                    fprintf ( stderr, "Please keep the buffer time between 1 and 604800 seconds\n" );
                    goto over;
                }

                break;
            case 'd':
                dbNum = atoi ( optarg );

                if ( dbNum < 0 || dbNum > 15 ) {
                    fprintf ( stderr, "Please keep database number between 0 and 15\n" );
                    goto over;
                }

                break;
            case 'h':
                PrintUsage ( argc, argv );
                goto over;
                break;
            case 'n':
                daemonize = 0;
                break;
            default:
                PrintUsage ( argc, argv );
                goto over;
                break;
        }
    }

    /* Validate input */
    if ( p == 0 ) {
        fprintf ( stderr, "Please set a port number\n" );
        goto over;
    }

    /* TODO move bufTime validation here */
    /******************/

    if ( daemonize ) {
        syslog ( LOG_INFO, "%s daemon starting up", DAEMON_NAME );

        if ( ( pid = fork () ) < 0 ) {
            syslog ( LOG_ERR, "Could not fork child process: %m" );
            goto over;
        } else if ( pid > 0 ) {
            FILE *pidfp = NULL;
            char pid_string[256];

            if ( ( pidfp = fopen ( PIDFILE, "w" ) ) ) {
                snprintf ( pid_string, 256, "%d", pid );
                fwrite ( pid_string, strlen ( pid_string ), 1, pidfp );
                fclose ( pidfp );
            }

            syslog ( LOG_NOTICE, "Child process successfully forked" );
            rc = EXIT_SUCCESS;

            goto over;
        }

        umask ( 0 );

        if ( ( sid = setsid () ) < 0 ) {
            syslog ( LOG_ERR, "Could not create new session: %m" );
            goto over;
        }

        if ( ( chdir ( "/" ) ) < 0 ) {
            syslog ( LOG_ERR, "Could not change working dir: %m" );
            goto over;
        }

        close ( STDIN_FILENO );
        close ( STDOUT_FILENO );
        close ( STDERR_FILENO );
    }

    if ( ( sockfd = setup_server ( p ) ) == -1 ) {
        syslog ( LOG_ERR, "Cannot set up server socket" );
        goto over;
    }

    if ( ( redis_ctx = httptee_redis_connect ( "127.0.0.1", 6379, bufTime, dbNum ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot initialise redis context" );
        goto over;
    }

    for ( ; ; ) {
        if ( ( recvlen = recvfrom ( sockfd, server_buffer, BUFSIZE, 0, ( struct sockaddr * ) &remaddr, &addrlen ) ) > 0 ) {
            hpr_p result = NULL;

            server_buffer[recvlen] = 0;

            if ( ( result = httptee_parse_result ( server_buffer ) ) == NULL ) {
                syslog ( LOG_ERR, "Cannot parse GET result" );
                syslog ( LOG_DEBUG, "%s", server_buffer );
            } else {
                //syslog ( LOG_INFO, "%s", httptee_get_string_after_last_slash ( result->base_path ) );

                httptee_redis_add_result ( redis_ctx, result );

                httptee_free_result ( result );
            }
        }
    }

    rc = EXIT_SUCCESS;
over:
    if ( redis_ctx ) httptee_redis_free ( redis_ctx );
    if ( sockfd != -1 ) close ( sockfd );

    return rc;
}
