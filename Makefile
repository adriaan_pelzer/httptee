#CFLAGS=-g -Wall -DDEBUG
CFLAGS=-Wall
LIBS=-lnids
LIBS_SERVER=-lhiredis

all: httptee

# HTTPTEE SERVER

httptee_server: Makefile httptee_server.o httptee_parse.o httptee_redis.o
	gcc -o httptee_server httptee_server.o httptee_parse.o httptee_redis.o ${LIBS_SERVER}

httptee_server.o: Makefile httptee_server.c httptee_server.h
	gcc ${CFLAGS} -c httptee_server.c -o httptee_server.o

httptee_parse.o: Makefile httptee_parse.c httptee_parse.h
	gcc ${CFLAGS} -c httptee_parse.c -o httptee_parse.o

httptee_redis.o: Makefile httptee_redis.c httptee_redis.h
	gcc ${CFLAGS} -c httptee_redis.c -o httptee_redis.o

# HTTPTEE SNIFFER

httptee: Makefile httptee.o httptee_nids_callback.o httptee_payloadqueue.o httptee_udpclient.o
	gcc -o httptee httptee.o httptee_nids_callback.o httptee_payloadqueue.o httptee_udpclient.o ${LIBS}

httptee.o: Makefile httptee.c httptee.h
	gcc ${CFLAGS} -c httptee.c -o httptee.o

httptee_ndis_callback.o: Makefile httptee_payloadqueue.c httptee_payloadqueue.h httptee_nids_callback.c httptee_nids_callback.h httptee.h
	gcc ${CFLAGS} -c httptee_nids_callback.c -o httptee_nids_callback.o

httptee_payloadqueue.o: Makefile httptee_payloadqueue.c httptee_payloadqueue.h httptee.h
	gcc ${CFLAGS} -c httptee_payloadqueue.c -o httptee_payloadqueue.o

httptee_udpclient.o: Makefile httptee_udpclient.c httptee_udpclient.h httptee.h
	gcc ${CFLAGS} -c httptee_udpclient.c -o httptee_udpclient.o

install:
	cp httptee /usr/bin

clean:
	rm *.o; rm httptee
