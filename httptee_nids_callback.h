#ifndef _HTTPTEE_NIDS_CALLBACK_H_
#define _HTTPTEE_NIDS_CALLBACK_H_

#include "httptee.h"

#define LOG_MAX 3000

void httptee_set_server_address ( const char *address );

void httptee_set_server_port ( int port );

void httptee_nids_tcp_handler ( struct tcp_stream *tcp_stream, void **this_time_not_needed );

#endif
