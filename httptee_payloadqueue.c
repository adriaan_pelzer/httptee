#include "httptee_payloadqueue.h"

char endpoints_cmp ( struct tuple4 e1, struct tuple4 e2 ) {
    char rc = -1;

    if ( e1.source != e2.source ) {
        goto over;
    }

    if ( e1.dest != e2.dest ) {
        goto over;
    }

    if ( e1.saddr != e2.saddr ) {
        goto over;
    }

    if ( e1.daddr != e2.daddr ) {
        goto over;
    }

    rc = 0;
over:
    return rc;
}

pqe_p httptee_payloadqueue_entry_new ( struct tuple4 endpoints ) {
    pqe_p pqe = NULL, _pqe = NULL;

    if ( ( _pqe = malloc ( sizeof ( struct httptee_payloadqueue_entry ) ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate memory for new payloadqueue_entry" );
        goto over;
    }

    memcpy ( &_pqe->endpoints, &endpoints, sizeof ( endpoints ) );

    _pqe->last_processed = time ( NULL );
    _pqe->client.payload_len = 0;
    _pqe->client.payload = NULL;
    _pqe->server.payload_len = 0;
    _pqe->server.payload = NULL;
    _pqe->prev = NULL;
    _pqe->next = NULL;

    pqe = _pqe;
over:
    if ( _pqe && ( pqe == NULL ) ) {
        httptee_payloadqueue_entry_free ( _pqe );
    }

    return pqe;
}

void httptee_payloadqueue_entry_free ( pqe_p pqe ) {
    if ( pqe ) {
        if ( pqe->client.payload ) {
            free ( pqe->client.payload );
        }

        if ( pqe->server.payload ) {
            free ( pqe->server.payload );
        }

        free ( pqe );
    }
}

char httptee_add_to_payloadqueue_entry ( pqe_p pqe, const char *new_payload, const size_t new_payload_len, char client ) {
    char rc = 0;
    struct httptee_payload *entity;

    if ( client ) {
        entity = &pqe->client;
    } else {
        entity = &pqe->server;
    }

    if ( ( entity->payload = realloc ( entity->payload, entity->payload_len + new_payload_len ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot allocate extra memory for new payload" );
        goto over;
    }

    memcpy ( ( char * ) ( ( size_t ) entity->payload + entity->payload_len ), new_payload, new_payload_len );
    entity->payload_len += new_payload_len;

    rc = 1;
over:
    return rc;
}

pqe_p httptee_find_payloadqueue_entry_by_endpoints ( pqe_p pq, struct tuple4 endpoints ) {
    pqe_p rc = NULL, _rc = NULL;

    if ( pq ) {
        _rc = pq->next;

        while ( _rc ) {
            if ( endpoints_cmp ( _rc->endpoints, endpoints ) == 0 ) {
                rc = _rc;
                goto over;
            }

            _rc = _rc->next;
        }
    }

    rc = NULL;
over:
    return rc;
}

pqe_p httptee_add_new_entry_to_payloadqueue ( pqe_p pq, struct tuple4 endpoints ) {
    pqe_p rc = NULL, last = NULL;

    if ( pq == NULL ) {
        syslog ( LOG_ERR, "Payload queue is null, cannot add to it" );
        goto over;
    }

    if ( ( rc = httptee_find_stale_payload ( pq ) ) != NULL ) {
        syslog ( LOG_DEBUG, "Found a stale payload - using it" );

        memcpy ( &rc->endpoints, &endpoints, sizeof ( endpoints ) );

        if ( rc->client.payload ) {
            free ( rc->client.payload );
            rc->client.payload = NULL;
            rc->client.payload_len = 0;
        }

        if ( rc->server.payload ) {
            free ( rc->server.payload );
            rc->server.payload = NULL;
            rc->server.payload_len = 0;
        }

        goto over;
    }

    last = pq;

    while ( last->next ) {
        last = last->next;
    }

    if ( ( last->next = httptee_payloadqueue_entry_new ( endpoints ) ) == NULL ) {
        syslog ( LOG_ERR, "Cannot create new payloadqueue entry" );
        goto over;
    }

    last->next->prev = last;

    rc = last->next;
over:
    return rc;
}

pqe_p httptee_add_to_payloadqueue ( pqe_p pq, struct tuple4 endpoints, const char *payload, const size_t payload_len, char client ) {
    pqe_p rc = NULL;
    pqe_p entry = NULL;

    if ( pq == NULL ) {
        syslog ( LOG_ERR, "Payload queue is null, cannot add to it" );
        goto over;
    }

    if ( ( entry = httptee_find_payloadqueue_entry_by_endpoints ( pq, endpoints ) ) == NULL ) {
        if ( ( entry = httptee_add_new_entry_to_payloadqueue ( pq, endpoints ) ) == NULL ) {
            syslog ( LOG_ERR, "Cannot add new entry to payloadqueue" );
            goto over;
        }
    }

    if ( httptee_add_to_payloadqueue_entry ( entry, payload, payload_len, client ) == 0 ) {
        syslog ( LOG_ERR, "Cannot add new payload to entry" );
        goto over;
    }

    rc = entry;
over:
    return rc;
}

char httptee_payload_splice ( struct httptee_payload *payload, size_t start_index, size_t end_index ) {
    char rc = 0;
    size_t i = 0;

    if ( payload && payload->payload_len ) {
        for ( i = end_index; i < payload->payload_len; i++ ) {
            payload->payload[i - end_index + start_index] = payload->payload[i];
        }

        if ( ( payload->payload = realloc ( payload->payload, payload->payload_len - end_index + start_index ) ) == NULL ) {
            syslog ( LOG_ERR, "Cannot reallocate payload memory in splice cycle" );
            goto over;
        }

        payload->payload_len = payload->payload_len - end_index + start_index;
    }

    rc = 1;
over:
    return rc;
}

void httptee_remove_entry_from_payloadqueue ( pqe_p *pq, pqe_p entry ) {
    if ( entry->prev ) entry->prev->next = entry->next;
    if ( entry->next ) entry->next->prev = entry->prev;

    /* Deal with case when first item deleted */
    if ( entry->prev == NULL ) *pq = entry->next;

    httptee_payloadqueue_entry_free ( entry );
}

char httptee_remove_from_payloadqueue ( pqe_p *pq, struct tuple4 endpoints ) {
    char rc = 0;
    pqe_p pqe = NULL;

    if ( *pq ) {
        if ( ( pqe = httptee_find_payloadqueue_entry_by_endpoints ( *pq, endpoints ) ) == NULL ) {
            syslog ( LOG_WARNING, "Cannot find payloadqueue_entry" );
            goto over;
        }

        httptee_remove_entry_from_payloadqueue ( pq, pqe );
    }

    rc = 1;
over:
    return rc;
}

#ifdef REMOVE_STALE_QUEUES
void httptee_remove_stale_payloads ( pqe_p *pq ) {
    pqe_p pqe = NULL, next = NULL;

    if ( *pq ) {
        pqe = *pq;

        do {
            next = pqe->next;

            if ( ( time ( NULL ) - pqe->last_processed ) > HTTPTEE_PAYLOADQUEUE_STALECHECK_PERIOD ) {
                httptee_remove_entry_from_payloadqueue ( pq, pqe );
            }
        } while ( ( pqe = next ) != NULL );
    }

    return;
}
#endif

pqe_p httptee_find_stale_payload ( pqe_p pq ) {
    pqe_p rc = NULL, candidate = NULL, next = NULL;

    if ( pq ) {
        candidate = pq;

        do {
            next = candidate->next;

            if ( ( time ( NULL ) - candidate->last_processed ) > HTTPTEE_PAYLOADQUEUE_STALECHECK_PERIOD ) {
                rc = candidate;
                goto over;
            }
        } while ( ( candidate = next ) != NULL );
    }

over:
    return rc;
}
